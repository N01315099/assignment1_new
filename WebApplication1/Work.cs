﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class Work
    {
        private List<string> avaliableD;
        private List<string> talentSp;
        private string positionV;
        private string communW;

        public Work()
        {

        }
        public List<string> AvaliableD
        {
            get { return avaliableD; }
            set { avaliableD = value; }
        }
        public List<string> TalentSp
        {
            get { return talentSp; }
            set { talentSp = value; }
        }
        public string CommunW
        {
            get { return communW; }
            set { communW = value; }
        }
        
        public string PositionV
        {
            get { return positionV; }
            set { positionV = value; }
          
        }
        
    }
}