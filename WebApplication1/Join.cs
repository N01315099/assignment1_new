﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1
{
    public class Join
    {
        public Volunteer volunteer;
        public Work work;

        public Join(Volunteer v, Work w)
        {
            volunteer = v;
            work = w;
        }

        public string PrintJoinFunc()
        {
            string value = "Hello:<br>";
            value += "volunteer name is :" + volunteer.personname + "<br/>";
            value += "work position: " + work.PositionV + "<br/>";
            return value;
        }
    }
}